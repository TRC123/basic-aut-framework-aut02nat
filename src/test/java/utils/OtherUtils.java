package utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.*;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Set;

public class OtherUtils {

    public static void printCookie(Cookie c) {
        System.out.println("Cookie= " + c.getName() + " : " + c.getValue());
        System.out.println(c.getExpiry());
    }

    public static void printCookies(WebDriver driver) {
        Set<Cookie> cookies = driver.manage().getCookies();
        System.out.println("The number of cookies found are: " + cookies.size());
        for (Cookie c : cookies) {
            printCookie(c);
        }
    }

    public static void takeScreenshot(WebDriver driver) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE); // tells it to take a screenshot
        File finalFile = new File(SeleniumUtils.screenshotPath + "\\screenshot-" + timestamp.getTime() +".png"); // tells us where to save it
        try {
            FileUtils.copyFile(screenshotFile, finalFile);
        } catch (
                IOException e) {
            System.out.println("File could not be saved");
        }
    }

    public static String sanitizeNullDbStrig(String dbResult) {
        if (dbResult == null) {
            return "";
        }
        return dbResult;
    }

    //read from properties file
    public static Properties readPropertiesFile(String path) throws IOException {
        InputStream input = new FileInputStream(path);
        Properties prop = new Properties();
        prop.load(input);
        return prop;
    }
}
