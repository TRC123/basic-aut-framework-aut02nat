package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegistrationPage {

    private WebDriver driver;
    WebDriverWait wait;

    /** Mandatory elements section */

    @FindBy(how = How.ID, using = "register-tab")
    WebElement registrationTab;

    @FindBy(how = How.ID, using = "inputFirstName")
    WebElement firstNameInput;

    @FindBy(how = How.ID, using = "inputLastName")
    WebElement lastNameInput;

    @FindBy(how = How.ID, using = "inputEmail")
    WebElement emailInput;

    @FindBy(how = How.ID, using = "inputUsername")
    WebElement usernameInput;

    @FindBy(how = How.ID, using = "inputPassword")
    WebElement passwordInput;

    @FindBy(how = How.ID, using = "inputPassword2")
    WebElement confirmPasswordInput;

    @FindBy(how = How.ID, using = "register-submit")
    WebElement submitButton;

    /** Error messages  section */

    @FindBy(how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[3]/div/div[2]/text()")
    WebElement firstNameError;

    @FindBy(how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[4]/div/div[2]/text()")
    WebElement lastNameError;

    @FindBy(how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[5]/div[1]/div[2]")
    WebElement emailError;

    @FindBy(how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[6]/div/div[2]/text()")
    WebElement usernameError;

    @FindBy(how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[7]/div/div[2]/text()")
    WebElement passwordError;

    @FindBy(how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[8]/div/div[2]")
    WebElement confirmPasswordError;


    public RegistrationPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void register(String firstName, String lastName, String email, String username, String password, String confirmPassword) {
        firstNameInput.click();
        firstNameInput.clear();
        firstNameInput.sendKeys(firstName);
        lastNameInput.click();
        lastNameInput.clear();
        lastNameInput.sendKeys(lastName);
        emailInput.click();
        emailInput.clear();
        emailInput.sendKeys(email);
        usernameInput.click();
        usernameInput.clear();
        usernameInput.sendKeys(email);
        passwordInput.click();
        passwordInput.clear();
        passwordInput.sendKeys(email);
        confirmPasswordInput.click();
        confirmPasswordInput.clear();
        confirmPasswordInput.sendKeys(email);
        wait.until(ExpectedConditions.elementToBeClickable(submitButton));
        submitButton.submit();
    }

    public void openRegisterPage(String hostname) {
        System.out.println("Open the next url:" + hostname + "/#/auth.html");
        driver.get(hostname + "/#/auth.html");
        wait.until(ExpectedConditions.elementToBeClickable(registrationTab));
        registrationTab.click();
    }

    public boolean checkErr(String error, String type) {
        if (type.equalsIgnoreCase("firstNameError"))
            return error.equals(firstNameError.getText());
        else if (type.equalsIgnoreCase("lastNameError"))
            return error.equals(lastNameError.getText());
        else if (type.equalsIgnoreCase("emailError"))
            return error.equals(emailError.getText());
        else if (type.equalsIgnoreCase("usernameError"))
            return error.equals(usernameError.getText());
        else if (type.equalsIgnoreCase("passwordError"))
            return error.equals(passwordError.getText());
        else if (type.equalsIgnoreCase("confirmPasswordError"))
            return error.equals(confirmPasswordError.getText());
        return false;
    }
}
