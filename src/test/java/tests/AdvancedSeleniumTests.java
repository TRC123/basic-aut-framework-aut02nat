package tests;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.OtherUtils;

import static utils.SeleniumUtils.waitForGenericElement;

public class AdvancedSeleniumTests extends BaseUITest {

    @Parameters({"searchKeyword"})
    @Test
    public void browserTest(String searchKeyword) {
        driver.get("https://google.com");
        driver.switchTo().frame(0);
        waitForGenericElement(driver, By.id("introAgreeButton"),15).click();
        WebElement input = driver.findElement(By.name("q"));
        input.sendKeys(searchKeyword);
        driver.findElement(By.name("btnK")).click();
        System.out.println("Verify search results");
    }


    @Test
    public void cookieTest() {
        driver.get("https://google.com");
        OtherUtils.printCookies(driver);
        try {
            OtherUtils.printCookie(driver.manage().getCookieNamed("CONSENT1"));
        } catch (Exception e) {
            System.out.println("Cookie not found !!!");
        }
        /** Folosim pentru a adauga un cookie extern, e.g.: cand ne da devul un cookie pentru autentificare */
        Cookie cookie = new Cookie("myCookie", "cookie123");
        driver.manage().addCookie(cookie);
        OtherUtils.printCookies(driver);
        driver.manage().deleteCookieNamed("CONSENT");
        driver.manage().deleteCookieNamed("myCookie");
        OtherUtils.printCookies(driver);
        driver.manage().deleteAllCookies();
        OtherUtils.printCookies(driver);
    }

    /**
     * Cookies tests
     */
    @Test
    public void cookieCheckTest() {
        driver.get(hostname + "/#/cookie.html");
        OtherUtils.printCookies(driver);
        WebElement setCookie = driver.findElement(By.id("set-cookie"));
        setCookie.click();
        OtherUtils.printCookies(driver);

    }
    /** de continuate ca si tema */



    /**
     * Screenshot tests
     */
    @Test
    public void screenshotTest() {
        driver.get("https://google.com");
        OtherUtils.takeScreenshot(driver);
    }

    /**
     * Alerts tests
     */
    @Test
    public void alertTest() {
        driver.get(hostname + "/#/alert.html");
        WebElement alertButton = driver.findElement(By.id("alert-trigger"));
        WebElement confirmButton = driver.findElement(By.id("confirm-trigger"));
        WebElement promptButton = driver.findElement(By.id("prompt-trigger"));
        alertButton.click();
        Alert alert = driver.switchTo().alert();
        System.out.println(alert.getText());
        alert.accept();
        confirmButton.click();
        alert = driver.switchTo().alert();
        System.out.println(alert.getText());
        alert.dismiss();
        promptButton.click();
        alert = driver.switchTo().alert();
        System.out.println(alert.getText());
        alert.sendKeys("orice");
        alert.accept();
    }

    /**
     * Hover button tests
     */
    @Test
    public void hoverButtonTest() {
        driver.get(hostname + "/#/hover.html");
        WebElement hoverbutton = driver.findElement(By.xpath("/html/body/div/div/div[2]/div/button")); //identificam butonul
        Actions actions = new Actions(driver); // legam mouse-ul de driver
//        actions = actions.moveToElement(hoverbutton); // schimbam focusul la hoverbutton
//        Action action = actions.build();
//        action.perform();
        actions.moveToElement(hoverbutton).build().perform(); // face acelasi lucru ca cele 3 randuri de mai sus
        WebElement itemMenu1 = driver.findElement(By.name("item 1"));
        System.out.println(itemMenu1.getText());
        itemMenu1.click();
    }

    /**
     * Stale button test ! foarte important este sa punem WebElement staleButton = driver.findElement(By.id("stale-button")); in for
     */
    @Test
    public void staleTest() {
        driver.get(hostname + "/#/stale.html");
        for (int i = 0; i < 20; i++) {
            WebElement staleButton = driver.findElement(By.id("stale-button"));
            staleButton.click();
        }
    }

    /**
     * Modal windows tests, foarte bulangii, daca niciuna din variante nu merge, putem ruga dev-ul sa puna id-uri la butoane pt o identificare mai sigura
     */
    @Test
    public void clickStealer() {
        driver.get(hostname + "/#/thieves.html");
        WebElement button = driver.findElement(By.id("the_button"));
        button.click();
        Actions action = new Actions(driver);
        //action.sendKeys(Keys.ESCAPE).build().perform(); // RIP, nu a mers la mine, dar actiunea este valida
        WebElement closeModal = driver.findElement(By.xpath("//*[@id=\"myModal\"]/div/div/div[3]/button"));
        //action.click(closeModal).build().perform();// RIP, nu a mers la mine, dar actiunea este valida
        WebElement closeModalX = driver.findElement(By.xpath("//*[@id=\"myModal\"]/div/div/div[1]/button"));
        //action.click(closeModalX).build().perform();// RIP, nu a mers la mine, dar actiunea este valida
        WebElement modalText = driver.findElement(By.xpath("//*[@id=\"myModal\"]/div/div/div[2]/p"));
        System.out.println(modalText.getText());
        //action.moveToElement(modalText).click(modalText).sendKeys(Keys.ESCAPE).build().perform();// RIP, nu a mers la mine, dar actiunea este valida
        button.click();
    }

    @Test
    public void checkboxTest() {
        driver.get(hostname + "/#/thieves.html");
        WebElement checkbox = driver.findElement(By.id("the_checkbox"));
        WebElement checkboxText = driver.findElement(By.xpath("//*[@id=\"div2\"]/label/span"));
        //checkbox.click(); // nu a mers cu click simplu, in astfel de scenarii trebuie sa folosim actions
        Actions actions = new Actions(driver);
        actions.click(checkbox).build().perform();
        System.out.println(checkboxText.getText());
    }
}
