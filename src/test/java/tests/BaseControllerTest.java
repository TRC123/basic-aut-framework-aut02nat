package tests;

import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;
import utils.OtherUtils;

import java.util.Properties;

public class BaseControllerTest {
    String apiHostname;
    String apiType;
    String apiVersion;

    @BeforeClass
    public void setUp() {
        try {
            Properties prop = OtherUtils.readPropertiesFile("src\\test\\java\\framework.properties");
            apiHostname = prop.getProperty("apiHostname");
            System.out.println("Use the next hostname:" + apiHostname);
            apiType = prop.getProperty("apiType");
            System.out.println("Use the next hostname:" + apiType);
            apiVersion = prop.getProperty("apiVersion");
            System.out.println("Use the next hostname:" + apiVersion);

//            set up rest-assured base hostname
            RestAssured.baseURI=apiHostname;
            RestAssured.useRelaxedHTTPSValidation();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}