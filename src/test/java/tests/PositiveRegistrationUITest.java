package tests;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.ProfilePage;
import pageObjects.RegistrationPage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class PositiveRegistrationUITest extends BaseUITest {

    @DataProvider(name = "registerDP")
    public Iterator<Object[]> registerDp() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        // firstname, lastname, email, username, password, confirmpassword
        dp.add(new String[]{"Giani", "Scandal", "giani@scandal.ro", "GianiScandal1", "gianiscandal", "gianiscandal"});
        return dp.iterator();
    }

        @Test(dataProvider = "registerDP")
        public void registerTest (String firstname, String lastname, String email, String username, String
                                 password, String confirmPW) {
            RegistrationPage registerPage = new RegistrationPage(driver);
            registerPage.openRegisterPage(hostname); //include deja wait-ul.
            registerPage.register(firstname, lastname, email, username, password, confirmPW);
            ProfilePage profilPage = new ProfilePage(driver);
            System.out.println("New user has been registered: " + profilPage.getUser());
            Assert.assertEquals(username, profilPage.getUser());
            profilPage.logOut();
//            Assert.assertTrue(registerPage.checkErr(firstnameErrMsg, "firstNameError"));
//            Assert.assertTrue(registerPage.checkErr(lastnameErrMsg, "lastNameError"));
//            Assert.assertTrue(registerPage.checkErr(emailErrMsg, "emailError"));
//            Assert.assertTrue(registerPage.checkErr(usernameErrMsg, "usernameError"));
//            Assert.assertTrue(registerPage.checkErr(pwErrMsg, "passwordError"));
//            Assert.assertTrue(registerPage.checkErr(confirmPWErrMsg, "confirmPasswordError"));

        }

    }
