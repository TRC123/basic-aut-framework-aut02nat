package tests;

import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;

public class CountryNameApiTest extends BaseControllerTest {
    @Test
    public void getCountryNameInfoTest() {
        Response response =
                given().log().uri().
                        when().
                        get(" /{apiType}/{apiVersion}/name/{countryName}", apiType, apiVersion, "Romania").
                        then().
                        statusCode(200).
                        contentType(JSON).
                        body("name[0]", equalTo("Romania")).
                        body("currencies[0][0].code", equalTo("RON")).
                        extract().
                        response();
        response.body().prettyPrint();
    }

    @Test
    public void verifyCountryCapital() {
        given().log().uri().
                when().
                get(" /{apiType}/{apiVersion}/name/{countryName}", apiType, apiVersion, "Romania").
                then().
                statusCode(200).
                contentType(JSON).
                body("capital[0]", equalTo("Bucharest"));
    }

    @Test
    public void verifyBoarders() {
        List<String> expectedBorders = Arrays.asList("BGR", "HUN", "MDA", "SRB", "UKR");
        List<String> borders = given().log().uri().
                when().
                get(" /{apiType}/{apiVersion}/name/{countryName}", apiType, apiVersion, "Romania").
                then().
                statusCode(200).
                contentType(JSON).
                extract().jsonPath().getList("borders[0]", String.class);
        System.out.println("Actual borders: " + borders);
        Assert.assertTrue(Arrays.deepEquals(borders.toArray(), expectedBorders.toArray()));
    }

    @Test
    public void verifySpellingV1() {
        String expectedAltSpelling = "România";
        List<String> altSpelling = given().log().uri().
                when().
                get(" /{apiType}/{apiVersion}/name/{countryName}", apiType, apiVersion, "Romania").
                then().
                statusCode(200).
                contentType(JSON).
                extract().jsonPath().getList("altSpellings[0]", String.class);
        System.out.println("Available spellings are: " + altSpelling);
        Assert.assertTrue(altSpelling.contains(expectedAltSpelling));
    }
/** Another example for the verifySpellingV1 test using hasItem */
    @Test
    public void verifySpellingV2() {
        given().log().uri().
                when().
                get(" /{apiType}/{apiVersion}/name/{countryName}", apiType, apiVersion, "Romania").
                then().
                statusCode(200).
                contentType(JSON).
                body("altSpellings[0]", hasItem("RO")).
                body("altSpellings[0]", hasItem("Rumania")).
                body("altSpellings[0]", hasItem("Roumania")).
                body("altSpellings[0]", hasItem("România"));
    }

    @Test
    public void verifyMultipleCurrencies() {
        given().log().uri().
                when().
                get(" /{apiType}/{apiVersion}/name/{countryName}", apiType, apiVersion, "Antarctica").
                then().
                statusCode(200).
                contentType(JSON).
                body("currencies[0][0].symbol",equalTo("$")).
                body("currencies[0].symbol",hasItem("£"));
    }
}
