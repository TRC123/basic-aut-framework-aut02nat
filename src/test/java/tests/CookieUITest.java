package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.OtherUtils;

public class CookieUITest extends BaseUITest {

    @Test
    public void cookieCheckTest() {
        driver.get(hostname + "/#/cookie.html");
        WebElement setCookie = driver.findElement(By.id("set-cookie"));
        WebElement removeCookie = driver.findElement(By.id("delete-cookie"));
        String cookieValue = driver.findElement(By.id("cookie-value")).getText();
        OtherUtils.printCookies(driver);
        Cookie myTestCookie = new Cookie("SampleCookie", "Test123");
        driver.manage().addCookie(myTestCookie);
        OtherUtils.printCookies(driver);
        Cookie newTestCookie = driver.manage().getCookieNamed("SampleCookie");
        Assert.assertEquals(newTestCookie.getValue(),"Test123");
        OtherUtils.printCookies(driver);
        driver.manage().deleteCookieNamed("newTestCookie");
        removeCookie.click();
        OtherUtils.printCookies(driver);
        Assert.assertEquals(cookieValue,"");
    }
}
